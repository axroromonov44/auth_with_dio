import 'package:auth_with_dio/bloc/auth/auth_bloc.dart';
import 'package:auth_with_dio/pages/auth_screen/login_screen.dart';
import 'package:auth_with_dio/pages/main_screen.dart';
import 'package:auth_with_dio/repo/user_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SimpleBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
  }
}

void main() {
  BlocOverrides.runZoned(
    () {
      final UserRepository userRepository = UserRepository();
      runApp(
        BlocProvider(
          create: (context) {
            return AuthBloc(userRepository)..add(AuthStartedEvent());
          },
          child: MyApp(
            userRepository: userRepository,
          ),
        ),
      );
    },
    blocObserver: SimpleBlocDelegate(),
  );
}

class MyApp extends StatefulWidget {
  final UserRepository userRepository;

  const MyApp({Key? key, required this.userRepository}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late AuthBloc _authBloc;
  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AuthBloc, AuthState>(
        bloc: _authBloc,
        builder: (context, state) {
          if (state is AuthAuthenticated) {
            return MainScreen();
          }
          if (state is AuthUnauthenticated) {
            return LoginScreen(
              userRepository: widget.userRepository,
            );
          }
          if (state is AuthLoading) {
            return Scaffold(
              body: CircularProgressIndicator(),
            );
          }
          return Scaffold(
            body: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
