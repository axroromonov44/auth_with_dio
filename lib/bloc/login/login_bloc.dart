import 'dart:async';

import 'package:auth_with_dio/bloc/auth/auth_bloc.dart';
import 'package:auth_with_dio/repo/user_repo.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthBloc authBloc;

  LoginBloc({
    required this.userRepository,
    required this.authBloc,
  }) : super(LoginInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event is LoginTapEvent) {
        emit(LoginLoadingState());
        try {
          final token = await userRepository.login(event.email, event.password);
          authBloc.add(AuthInEvent(token: token));
          emit(LoginInitial());
        } catch (e) {
          emit(LoginFailureState(error: e.toString()));
        }
      }
    });
  }
}
