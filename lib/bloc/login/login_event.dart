part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginTapEvent extends LoginEvent {
  final String email;
  final String password;

  const LoginTapEvent({required this.email,required this.password});

  @override
  List<Object?> get props => [email, password];

  @override
  String toString() => 'LoginTap {email: $email , password : $password}';
}
