part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}
class AuthStartedEvent extends AuthEvent{
  @override
  List<Object?> get props => [];
}
class AuthInEvent extends AuthEvent{
  final String token;

  const AuthInEvent({required this.token});

  @override
  List<Object?> get props => [token];

  @override
  String toString() => 'Auth {$token}';
}
class LogOutEvent extends AuthEvent{
  @override
  List<Object?> get props => [];
}
