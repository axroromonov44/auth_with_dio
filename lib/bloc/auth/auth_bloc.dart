import 'dart:async';

import 'package:auth_with_dio/repo/user_repo.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository repository;
  AuthBloc(this.repository) : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      if(event is AuthStartedEvent)  {
        final bool hasToken = await repository.hasToken();
        if(hasToken){
          emit(AuthAuthenticated());
        }else{
          emit(AuthUnauthenticated());
        }
      }
    });
    on<AuthInEvent>((event, emit) async {
      emit(AuthLoading());
      await repository.persistToken(event.token);
      emit(AuthAuthenticated());
    });
    on<LogOutEvent>((event, emit) async {
      emit(AuthLoading());
      await repository.deleteToken();
      emit(AuthUnauthenticated());
    });
  }
}
