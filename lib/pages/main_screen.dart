import 'package:auth_with_dio/bloc/auth/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF6584f7),
        title: const Text("AUTH WITH REST"),
        leading: const Padding(
          padding: EdgeInsets.all(10.0),
          child: CircleAvatar(
            backgroundImage: NetworkImage(
                "https://yt3.ggpht.com/yti/ANoDKi5R5eJSjZigdWmIcZKFAtqwG4svMcAAN0Iyvw4j=s108-c-k-c0x00ffffff-no-rj"),
          ),
        ),
        actions: [
          IconButton(icon: const Icon(Icons.logout), onPressed: () {
            BlocProvider.of<AuthBloc>(context).add(
              LogOutEvent(),
            );
          })
        ],
      ),
      body: const Center(
        child: Text(
          'Main Screen',
        ),
      ),
    );
  }
}
