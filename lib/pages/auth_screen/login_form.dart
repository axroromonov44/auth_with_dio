import 'package:auth_with_dio/bloc/login/login_bloc.dart';
import 'package:auth_with_dio/repo/user_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginForm extends StatefulWidget {
  final UserRepository userRepository;

  const LoginForm({Key? key, required this.userRepository}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool isLoading = false;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailureState) {
          isLoading = false;
          Scaffold.of(context).showSnackBar(
            const SnackBar(
              content: Text("Login failed."),
              backgroundColor: Color(0xFFE74C3C),
            ),
          );
        }
      },
      builder: (context, state) {
        if (state is LoginLoadingState) {
          isLoading = true;
        }
        return Padding(
          padding: const EdgeInsets.only(left: 16,right: 16,),
          child: Form(
            child: Column(
              children: [
                SizedBox(
                  height: 200.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text(
                        "Welcome back",
                        style: TextStyle(
                          color: Color(0xFF6584f7),
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30.0,
                ),
                TextFormField(
                  style: const TextStyle(
                      fontSize: 14.0,
                      color: Color(0xFF061857),
                      fontWeight: FontWeight.bold),
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black12),
                        borderRadius: BorderRadius.circular(30.0)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Color(0xFF6584f7)),
                        borderRadius: BorderRadius.circular(30.0)),
                    contentPadding:
                        const EdgeInsets.only(left: 10.0, right: 10.0),
                    labelText: "E-Mail",
                    hintStyle: const TextStyle(
                        fontSize: 12.0,
                        color: Color(0xFFb4bdce),
                        fontWeight: FontWeight.w500),
                    labelStyle: const TextStyle(
                        fontSize: 12.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500),
                  ),
                  autocorrect: false,
                ),
                const SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  style: const TextStyle(
                      fontSize: 14.0,
                      color: Color(0xFF061857),
                      fontWeight: FontWeight.bold),
                  controller: _passwordController,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.black12),
                        borderRadius: BorderRadius.circular(30.0)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Color(0xFF6584f7)),
                        borderRadius: BorderRadius.circular(30.0)),
                    contentPadding:
                        const EdgeInsets.only(left: 10.0, right: 10.0),
                    labelText: "Password",
                    hintStyle: const TextStyle(
                        fontSize: 12.0,
                        color: Color(0xFFb4bdce),
                        fontWeight: FontWeight.w500),
                    labelStyle: const TextStyle(
                        fontSize: 12.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500),
                  ),
                  autocorrect: false,
                  obscureText: true,
                ),
                const SizedBox(
                  height: 30.0,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    child: const Text(
                      "Forget password?",
                      style: TextStyle(color: Colors.black45, fontSize: 12.0),
                    ),
                    onTap: () {},
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 30,
                    bottom: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: 45,
                        child: isLoading
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Column(
                                      children: const [
                                        SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CupertinoActivityIndicator(),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )
                            : MaterialButton(
                                onPressed: () {
                                  BlocProvider.of<LoginBloc>(context).add(
                                    LoginTapEvent(
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                    ),
                                  );
                                },
                                child: const Text(
                                  'LOG IN',
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                                color: const Color(0xFF6584f7),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)),
                              ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        padding: const EdgeInsets.only(bottom: 30.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              "Don't have an account?",
                              style: TextStyle(color: Color(0xFFb4bdce)),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            GestureDetector(
                                onTap: () {},
                                child: const Text(
                                  "Register",
                                  style: TextStyle(
                                      color: Color(0xFF6584f7),
                                      fontWeight: FontWeight.bold),
                                ))
                          ],
                        )),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
