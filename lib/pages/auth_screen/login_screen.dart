import 'package:auth_with_dio/bloc/auth/auth_bloc.dart';
import 'package:auth_with_dio/bloc/login/login_bloc.dart';
import 'package:auth_with_dio/pages/auth_screen/login_form.dart';
import 'package:auth_with_dio/repo/user_repo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  final UserRepository userRepository;

  const LoginScreen({Key? key, required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) {
          return LoginBloc(userRepository: userRepository, authBloc: BlocProvider.of<AuthBloc>(context));
        },
         child: LoginForm(userRepository: userRepository),
      ),
    );
  }
}


